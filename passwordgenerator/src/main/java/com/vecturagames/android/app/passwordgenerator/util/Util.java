package com.vecturagames.android.app.passwordgenerator.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.pm.PackageInfoCompat;

import com.vecturagames.android.app.passwordgenerator.BuildConfig;
import com.vecturagames.android.app.passwordgenerator.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;
import java.util.Locale;

public class Util {

    public static DisplayMetrics getDisplayMetrics(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics;
    }

    public static Point getDisplaySize(Activity activity) {
        DisplayMetrics displayMetrics = getDisplayMetrics(activity);

        Point size = new Point();
        size.x = displayMetrics.widthPixels;
        size.y = displayMetrics.heightPixels;

        return size;
    }

    public static int dpToPx(DisplayMetrics displayMetrics, int dp) {
        return dpToPx(displayMetrics, dp, true);
    }

    public static int dpToPx(DisplayMetrics displayMetrics, int dp, boolean xdpi) {
        float dpi = xdpi ? displayMetrics.xdpi : displayMetrics.ydpi;
        return Math.round(dp * (dpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int pxToDp(DisplayMetrics displayMetrics, int px) {
        return pxToDp(displayMetrics, px, true);
    }

    public static int pxToDp(DisplayMetrics displayMetrics, int px, boolean xdpi) {
        float dpi = xdpi ? displayMetrics.xdpi : displayMetrics.ydpi;
        return Math.round(px / (dpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static String getVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        }
        catch (Exception e) {
            return context.getString(R.string.other_not_available);
        }
    }

    public static int getVersionCode(Context context) {
        try {
            return (int)(PackageInfoCompat.getLongVersionCode(context.getPackageManager().getPackageInfo(context.getPackageName(), 0)));
        }
        catch (Exception e) {
            return -1;
        }
    }

    public static String getBuildTime(Context context) {
        return Util.formatTime(context, new Date(BuildConfig.BUILD_TIME), false, false);
    }

    public static void openPlayStore(Context context, String playStorePackage, int flags) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + playStorePackage));
            intent.setFlags(flags);
            context.startActivity(intent);
        }
        catch (ActivityNotFoundException e) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + playStorePackage));
                intent.setFlags(flags);
                context.startActivity(intent);
            }
            catch (ActivityNotFoundException ee) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.dialog_no_internet_browser);
                builder.setPositiveButton(context.getString(R.string.dialog_button_ok), null);
                builder.show();
            }
        }
    }

    public static void openInternetBrowser(Context context, String link, int flags) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            intent.setFlags(flags);
            context.startActivity(intent);
        }
        catch (ActivityNotFoundException e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(R.string.dialog_no_internet_browser);
            builder.setPositiveButton(context.getString(R.string.dialog_button_ok), null);
            builder.show();
        }
    }

    public static void openEmailApplication(Context context, String link, int flags) {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setFlags(flags);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[] {link});
            Intent mailer = Intent.createChooser(intent, null);
            context.startActivity(mailer);
        }
        catch (ActivityNotFoundException e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(R.string.dialog_no_email_application);
            builder.setPositiveButton(context.getString(R.string.dialog_button_ok), null);
            builder.show();
        }
    }

    private static String getPattern(Context c, Locale locale, boolean withTime) {
        String pattern = DateTimeFormat.patternForStyle("SM", locale);
        if (!withTime) {
            pattern = DateTimeFormat.patternForStyle("S-", locale);
        }

        if (!pattern.contains("yyyy")) {
            pattern = pattern.replace("yy", "yyyy");
        }

        if (DateFormat.is24HourFormat(c)) {
            pattern = pattern.replace("h", "H");
            if (pattern.contains("a")) {
                pattern = pattern.replace("a", "");
            }
        }
        else {
            pattern = pattern.replace("H", "h");
            if (!pattern.contains("a")) {
                int i = pattern.lastIndexOf("s");
                if (i > -1) {
                    pattern = pattern.substring(0, i + 1) + " aa " + pattern.substring(i + 1);
                }
            }
            else {
                pattern = pattern.replace("a", "aa");
            }
        }

        return pattern;
    }

    @SuppressWarnings("deprecation")
    private static String formatTime(Context c, DateTime t, boolean withTime, boolean addUtcOffset) {
        String time;

        Locale locale;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = c.getResources().getConfiguration().getLocales().get(0);
        }
        else {
            locale = c.getResources().getConfiguration().locale;
        }

        String pattern = getPattern(c, locale, withTime);
        DateTimeFormatter format = DateTimeFormat.forPattern(pattern);

        DateTimeZone timeZone = DateTimeZone.UTC;
        time = t.toDateTime(timeZone).toString(format);

        if (addUtcOffset) {
            long timeZoneOffsetMillis = timeZone.getOffset(t.getMillis());
            long hours = timeZoneOffsetMillis / (1000L * 60L * 60L);
            long minutes = Math.abs((timeZoneOffsetMillis / (1000L * 60L)) % 60L);

            String hoursStr = (hours != 0 ? (hours >= 0 ? "+" : "-") : "±") + (hours <= 9 && hours >= -9 ? "0" : "") + Math.abs(hours);
            String minutesStr = (minutes <= 9 ? "0" : "") + minutes;

            time += " (UTC" + hoursStr + ":" + minutesStr + ")";
        }

        return time;
    }

    public static String formatTime(Context c, Date date, boolean withTime, boolean addUtcOffset) {
        DateTime t = new DateTime(date);
        return formatTime(c, t, withTime, addUtcOffset);
    }

    public static int clamp(int val, int min, int max) {
        return Math.max(min, Math.min(max, val));
    }

}
