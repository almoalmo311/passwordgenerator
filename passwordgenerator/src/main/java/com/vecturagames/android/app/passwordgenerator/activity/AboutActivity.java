package com.vecturagames.android.app.passwordgenerator.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.vecturagames.android.app.passwordgenerator.R;
import com.vecturagames.android.app.passwordgenerator.preference.AppSettings;
import com.vecturagames.android.app.passwordgenerator.util.Util;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(AppSettings.getInstance().getAppThemeId());
        setContentView(R.layout.activity_about);

        TextView textViewWebsiteValue = (TextView)findViewById(R.id.textViewWebsiteValue);
        textViewWebsiteValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.openInternetBrowser(AboutActivity.this, getString(R.string.other_website), 0);
            }
        });

        TextView textViewPrivacyPolicyValue = (TextView)findViewById(R.id.textViewPrivacyPolicyValue);
        textViewPrivacyPolicyValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.openInternetBrowser(AboutActivity.this, getString(R.string.other_privacy_policy_website), 0);
            }
        });

        TextView textViewEmailValue = (TextView)findViewById(R.id.textViewEmailValue);
        textViewEmailValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.openEmailApplication(AboutActivity.this, getString(R.string.other_contact_email), 0);
            }
        });

        TextView textViewSourceCodeValue = (TextView)findViewById(R.id.textViewSourceCodeValue);
        textViewSourceCodeValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.openInternetBrowser(AboutActivity.this, getString(R.string.other_source_code_repository), 0);
            }
        });

        TextView textViewVersionValue = (TextView)findViewById(R.id.textViewVersionValue);
        textViewVersionValue.setText(Util.getVersionName(this));

        TextView textViewBuildNumberValue = (TextView)findViewById(R.id.textViewBuildNumberValue);
        String buildTime = Util.getBuildTime(this);
        int versionCode = Util.getVersionCode(this);

        if (versionCode > -1) {
            textViewBuildNumberValue.setText(String.valueOf(versionCode) + (!buildTime.equals("") ? " (" + buildTime + ")" : ""));
        }
        else {
            textViewBuildNumberValue.setText(getString(R.string.other_not_available));
        }
    }

}
